package recursion

object Main extends App {

  def fact(n: BigInt): BigInt = {
    if (n <= 1) 1 else n * fact(n - 1)
  }
  
  def combination(n : BigInt, k : Int ) : BigInt = {
    fact(n) / (fact(k)*fact(n-k))
  }
  println("Poker hand probability: " + combination(52,5))
}
