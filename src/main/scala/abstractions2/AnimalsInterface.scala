package abstractions2

trait Animal {
  def makeSound 
}

class Dog extends Animal {
  def makeSound {
    println("bark!")
  }
}

class Cat extends Animal {
  def makeSound {
    println("meaw!")
  }
}

object Main extends App {
  def animals = List(new Dog(), new Cat())
  for (each <- animals) {
    each.makeSound
  }
}