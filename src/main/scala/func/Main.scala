package func

object Main extends App {
  
  def makePowerFunc(pwr:Int) : (Int => Int) = {
    
    def f( v : Int) = {
       var res : Int = 1
       for(_ <- 1 to pwr) {
         res *= v
       }
       res
    }
    
    f
  }
  
  val sqr = makePowerFunc(2)
  val cube =  makePowerFunc(3)
  
  println("square of 2 is: " + sqr(2))
  println("cube of 2 is: " + cube(2))

}