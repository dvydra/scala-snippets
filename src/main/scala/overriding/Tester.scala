package overriding

class A {
  def foo() = {
    println("A:foo()")
  }
}

class B extends A {
  override def foo() = {
    println("B:foo()")
    super.foo
  }
}

class C extends B { 
	override def foo() = {
		super.foo
		println("C:foo")
	}
}

object Tester extends App {
  
  val c = new C
  c.foo()

}